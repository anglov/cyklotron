package pl.edu.pw.fizyka.pojava.f8;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JApplet;

//Class for creation a main applet witch contain drawing area, charts and menu

public class ContentApplet extends JApplet {
	private static final long serialVersionUID = 228393632367745605L;
	private List<AnimationPoint> pointList;
	private DrawingPanel drawArea;
	private ChartsPanel charts;
	private MenuPanel menu;
	public DrawingPanel getDrawArea() {
		return drawArea;
	}

	public ChartsPanel getCharts() {
		return charts;
	}

	public MenuPanel getMenu() {
		return menu;
	}
	
	public List<AnimationPoint> getPointList() {
		return pointList;
	}

	public void setPointList(List<AnimationPoint> pointList) {
		this.pointList = pointList;
	}
	
    public void init( )
    {
    	createGUI();
    }
    
    //Method initialize draw area first time
    
    public void start() {
    	drawArea.initialiseImage();
    }

    void createGUI() {
    	setSize(1024,768);
		getContentPane().setLayout(new BorderLayout());
		createChats();
		createMenu();
		createDrawArea();
    }
    
    void createChats(){
		charts = new ChartsPanel(this);
		getContentPane().add(charts, BorderLayout.SOUTH);
    }
    
    void createMenu(){
    	menu = new MenuPanel(this);
		menu.setLayout(new BoxLayout(menu, BoxLayout.PAGE_AXIS));
		menu.setPreferredSize(new Dimension(310,0));
		getContentPane().add(menu, BorderLayout.EAST);
		
    }
    
    void createDrawArea(){
		drawArea = new DrawingPanel(this);
		getContentPane().add(drawArea, BorderLayout.CENTER);
    }

 }
