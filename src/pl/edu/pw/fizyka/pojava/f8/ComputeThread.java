package pl.edu.pw.fizyka.pojava.f8;

import java.util.ArrayList;

import javax.swing.JOptionPane;

//Class calculating the coordinates of the track, velocity, time, energy and 
//force vector coordinates. Main part of animation.

public class ComputeThread extends Thread {
	private ContentApplet parrent;
	double chargeOverMass, chargeFactor, electricField, magneticField, electricFrequency;
	static private enum State {LEFT_SIDE, CENTER_FROM_LEFT, CENTER_FROM_RIGHT, RIGHT_SIDE}
	static State actualState = State.CENTER_FROM_RIGHT;
	private volatile boolean running = true;
	static private boolean paused = false;
	public final static int space = 10;
	public final static double spaceUnit = 0.001;
	public final static double maxRadius = 18 * space*spaceUnit;
	final static int initialVelocity = 2500000;
	final static int vectorLength = 15;
	static int delay = 50;
	static AnimationPoint current, next;
	static double radius = 0;
	static int angle = 90;
	static double position = 0;
	static double previousVelocity = initialVelocity;
	static int spaceIterator = 0;

	public ComputeThread(ContentApplet parrent, double chargeOverMass, double chargeFactor,
			double electricField, double magneticField, double electricFrequency) {
		this.parrent = parrent;
		this.chargeOverMass = chargeOverMass;
		this.chargeFactor = chargeFactor;
		this.electricField = electricField;
		this.magneticField = magneticField;
		if(electricFrequency != 0 )
		this.electricFrequency = Math.pow(10, -6)/electricFrequency;
		else this.electricFrequency = 0;
	}
	
	public static void setDelay(int delay) {
		ComputeThread.delay = delay;
	}
	
	static void clearVariables() {
		paused = false;
		current = null;
		next = null;
		paused = false;
		radius = 0;
		angle = 90;
		previousVelocity = initialVelocity;
		actualState = State.CENTER_FROM_RIGHT;
		spaceIterator = 0;
		position = 0;
	}

	//Method dynamically drawn track

	public void run() {
		if (!paused) {
			double velocity = initialVelocity;
			double energy = velocity*velocity*chargeFactor / (2* chargeOverMass);
			current = new AnimationPoint(0, 0, 0, velocity, energy);
			synchronized (parrent) {
				parrent.setPointList(new ArrayList<AnimationPoint>());
				parrent.getPointList().add(current);
			}
		} else {
			paused = false;
		}
		while (running) {
			try {
				next = generatePoint(current);
				if (next.velocity < 0)
				{
					running = false;
					endAnimation();
					JOptionPane.showMessageDialog(parrent, 
							"Eksperyment nieudany - pr\u0119dko\u015B\u0107 mniejsza od zera \n" +
							"Zmie\u0144 cz\u0119stotliwo\u015B\u0107", 
							"Eksperyment nieudany", JOptionPane.ERROR_MESSAGE);		
				}
				synchronized (parrent) {
					parrent.getPointList().add(next);
				}
				parrent.getDrawArea().drawPoint(current, next);
				parrent.getDrawArea().repaint();
				current = next;
				sleep(delay);
				if (Math.abs(position) > maxRadius) {
					running = false;
					endAnimation();
				}
			} catch (InterruptedException e) {
				running = false;
			}
		}
	}
	
	private void endAnimation() {
		parrent.getMenu().stopButtonDisablingAction();
		ChartsPanel.timer.stop();
	}
	

	public void pause() {
		paused = true;
		interrupt();
	}

	//Method of adding points to the track

	private AnimationPoint generatePoint(final AnimationPoint previous) {
		double velocity = 0, energy = 0;
		double x = 0, y = 0, time = 0; 
		switch (actualState) {
		case CENTER_FROM_LEFT:
			x = spaceIterator * spaceUnit;
			y = previous.getyFromCenter();
			spaceIterator+=2;
			if (electricFrequency != 0 && (int)(previous.time / electricFrequency) % 2 == 0){
				velocity = previousVelocity - 
				Math.sqrt (2*chargeOverMass*electricField*(space*spaceUnit+x));
				parrent.getDrawArea().setVectorEnd(x, y);
				parrent.getDrawArea().setVectorStart(x + vectorLength*spaceUnit, y);
			}
			else { 
				velocity = previousVelocity + 
						Math.sqrt (2*chargeOverMass*electricField*(space*spaceUnit+x));
				parrent.getDrawArea().setVectorStart(x, y);
				parrent.getDrawArea().setVectorEnd(x + vectorLength*spaceUnit, y);
			}
				
			energy = velocity*velocity*chargeFactor / (2* chargeOverMass);
			time = previous.getTime() + spaceUnit/velocity;
			parrent.getMenu().setVelocityValue(velocity);
			parrent.getMenu().setEnergyValue(energy);
			if (spaceIterator >= 0) {
				actualState = State.RIGHT_SIDE;
				radius = velocity/(magneticField*chargeOverMass);
				position = y;				
			}
			break;
		case CENTER_FROM_RIGHT: {
			x = spaceIterator * spaceUnit;
			y = previous.getyFromCenter();
			spaceIterator-=2;
			if (electricFrequency == 0 || (int)(previous.time / electricFrequency) % 2 == 0 ){
				velocity = previousVelocity +
						Math.sqrt (2*chargeOverMass*electricField*(-x));
				parrent.getDrawArea().setVectorStart(x, y);
				parrent.getDrawArea().setVectorEnd(x - vectorLength * spaceUnit, y);

			}
			else {
				velocity = previousVelocity - 
					Math.sqrt (2*chargeOverMass*electricField*(-x));
				parrent.getDrawArea().setVectorEnd(x, y);
				parrent.getDrawArea().setVectorStart(x - vectorLength * spaceUnit, y);

			}
			energy = velocity*velocity*chargeFactor / (2* chargeOverMass);
			time = previous.getTime() + spaceUnit/velocity;
			parrent.getMenu().setVelocityValue(velocity);
			parrent.getMenu().setEnergyValue(energy);
			if (spaceIterator <= -space) {
				actualState = State.LEFT_SIDE;
				radius = velocity/(magneticField*chargeOverMass);
				position = y;
			}
			break;
		}
		case LEFT_SIDE:
			x = Math.cos( angle * Math.PI / 180 )*radius - space * spaceUnit;
			y = Math.sin( angle * Math.PI / 180 )*radius + position - radius;
			velocity = previous.getVelocity();
			energy = previous.getEnergy();
			time = previous.getTime() + (3 * Math.PI / 180) * radius/velocity;
			parrent.getDrawArea().setVectorStart(x, y);
			parrent.getDrawArea().setVectorEnd(
					x- vectorLength *spaceUnit * Math.cos(angle * Math.PI / 180 ), 
					y - 15 * spaceUnit * Math.sin(angle * Math.PI / 180 ));
			angle+=3;
			if (angle >= 270) {
				previousVelocity = velocity;
				actualState = State.CENTER_FROM_LEFT;
				angle -= 360;
			}
			break;
		case RIGHT_SIDE:
			x = Math.cos( angle * Math.PI / 180 ) * radius;
			y = Math.sin( angle * Math.PI / 180 ) * radius + position + radius;
			velocity = previous.getVelocity();
			energy = previous.getEnergy();
			time = previous.getTime() + (3 * Math.PI / 180) * radius/velocity;
			parrent.getDrawArea().setVectorStart(x, y);
			parrent.getDrawArea().setVectorEnd(
					x - vectorLength *spaceUnit * Math.cos(angle * Math.PI / 180 ), 
					y-15 * spaceUnit * Math.sin(angle * Math.PI / 180 ));
			angle+=3;
			if (angle >= 90) {
				previousVelocity = velocity;
				actualState = State.CENTER_FROM_RIGHT;
			}
			break;
		default:
			break;
		}
		AnimationPoint point = new AnimationPoint(x, y, time, velocity, energy);
		return point;
	}

}