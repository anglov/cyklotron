package pl.edu.pw.fizyka.pojava.f8;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

//Class creates dynamic charts - velocity chart & energy chart

public class ChartsPanel extends JPanel {
	private static final long serialVersionUID = -7229177031943103216L;
	private ContentApplet parrent;
	private ChartPanel velocityChartPanel, energyChartPanel;
	private JFreeChart velocityChart, energyChart;
	public static final Timer timer = new Timer(2000, null);
	public ChartsPanel(ContentApplet parrent) {
		this.parrent = parrent;
		setLayout(new GridLayout(1,2));
		setPreferredSize(new Dimension(400,300));
		createNullCharts();
		velocityChartPanel = new ChartPanel(velocityChart);
		energyChartPanel = new ChartPanel(energyChart);
		add(velocityChartPanel);
		add(energyChartPanel);
	}
	
	//Method of dynamically changing graph using timer
	
	public void animation() {
		timer.start();
		timer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				createCharts();
				velocityChartPanel.setChart(velocityChart);
				energyChartPanel.setChart(energyChart);
			}
		});

	}
	
	private void createCharts()
	{
		AnimationPoint[] pointArray = getPointArray();
		if (pointArray == null)
			createNullCharts();
		else {
		createVelocityChart(pointArray);
		createEnergyChart(pointArray);
		}
	}
	
	private AnimationPoint[] getPointArray() {
		if (parrent.getPointList() != null) {
			synchronized (parrent) {
				return parrent.getPointList().toArray(new AnimationPoint[0]);
			}
		}
		return null;
	}

	private void createVelocityChart (AnimationPoint[] pointArray) {
		XYSeries velocitySeries = new XYSeries("velocity");
		for (int ii = 0; ii < pointArray.length; ++ii) {
			velocitySeries.add(pointArray[ii].time*1000000000, pointArray[ii].velocity/1000);
		}
		XYSeriesCollection velocitySeriesCollection = new XYSeriesCollection();
		velocitySeriesCollection.addSeries(velocitySeries);
		velocityChart = ChartFactory.createXYLineChart(
				"Zale\u017Cno\u015B\u0107 pr\u0119dko\u015Bci od czasu", "t [ns]", "v [km/s]",
				velocitySeriesCollection, PlotOrientation.VERTICAL, false, false, false);
	}
	
	private void createEnergyChart (AnimationPoint[] pointArray) {
		XYSeries energySeries = new XYSeries("energy");
		for (int ii = 0; ii < pointArray.length; ++ii) {
			energySeries.add(pointArray[ii].time*1000000000, pointArray[ii].energy);
		}
		XYSeriesCollection energySeriesCollection = new XYSeriesCollection();
		energySeriesCollection.addSeries(energySeries);
		energyChart = ChartFactory.createXYLineChart(
				"Zale\u017Cno\u015B\u0107 energii od czasu", "t [ns]", "E [eV]",
				energySeriesCollection, PlotOrientation.VERTICAL, false, false, false);
	}
	
	private void createNullVelocityChart () {
		velocityChart = ChartFactory.createXYLineChart
                ("Zale\u017Cno\u015B\u0107 pr\u0119dko\u015Bci od czasu", "t [ns]", "v [km/s]",
                null, PlotOrientation.VERTICAL, false, false, false);
	}
	
	private void createNullEnergyChart () {
		energyChart = ChartFactory.createXYLineChart
                ("Zale\u017Cno\u015B\u0107 energii od czasu", "t [ns]", "E [eV]",
                null, PlotOrientation.VERTICAL, false, false, false);
	}

	private void createNullCharts () {
		createNullVelocityChart();
		createNullEnergyChart();
		}
	
	//Method creates and set a blank charts when it's needed 
	
	public void setNullCharts () {
		createNullCharts();
		velocityChartPanel.setChart(velocityChart);
		energyChartPanel.setChart(energyChart);
	}
}
