package pl.edu.pw.fizyka.pojava.f8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

//Class creates all panel elements & events

public class MenuPanel extends JPanel {
	private static final long serialVersionUID = -7727041379235078148L;
	private ContentApplet parrent = new ContentApplet();
	final static double chargeOverMassElectron = 95813397.1291866;
	final static double chargeElectron = 1.602176;
	final static double massElectron = 9.10938;
	private double electricField, magneticField, electricFieldFrequency;
	private double particleChargeOverMass, chargeFactor;
	private JButton startButton, stopButton, pauseButton, clearButton;
	private JButton automaticParametersButton;
	private JButton fasterButton, slowerButton;
	private JTextField magneticFieldSetter, electricFieldSetter, electricFieldFrequencySetter;
	private JTextField particleMassSetter, particleChargeSetter;
	private JTextField velocityValue, energyValue;
	private JComboBox<String> typeOfParticle;
	private ComputeThread computePoints;
	private final int 
		ELECTRON_SELECTED=  0, PROTON_SELECTED = 1, DEUTER_SELECTED = 2, ANOTHER_SELECTED = 3;
	private int speedSelected = 2;
	private int[] speedOption = {20, 35, 50, 75, 100};
	
	public void setVelocityValue(double velocity) {
		velocityValue.setText((velocity/1000) + "");
	}

	public void setEnergyValue(double energy) {
		energyValue.setText(energy + "");
	}
	public MenuPanel(ContentApplet parrent) {
		this.parrent=parrent;
		createTtileSection();
		add(Box.createVerticalStrut(15));
		createMagneticFieldSection();
		add(Box.createVerticalStrut(5));
		createElectricFieldSection();
		add(Box.createVerticalStrut(5));
		createElectricFieldFrequencySection();
		add(Box.createVerticalStrut(5));
		createTypeOfParticleSection();
		add(Box.createVerticalGlue());
		createParticleSection();
		add(Box.createVerticalStrut(5));
		createControlButtonsSection();
		add(Box.createVerticalStrut(5));
		createAutomaticParamaterButtonSection();
		createAnimationSpeedSection();
		add(Box.createVerticalGlue());
		createVelocitySection();
		add(Box.createVerticalStrut(5));
		createEnergySection();
		setBorder(BorderFactory.createLineBorder(Color.black));
		}
	
	void createTtileSection() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setPreferredSize(new Dimension(300,0));
		JLabel menuCaption = new JLabel("MENU");
		menuCaption.setAlignmentX(CENTER_ALIGNMENT);
		add(menuCaption);

	}
	
	void createMagneticFieldSection() {
		JPanel magneticFieldPanel = new JPanel();
		magneticFieldPanel.setLayout(new BoxLayout(magneticFieldPanel, BoxLayout.LINE_AXIS));
		add(magneticFieldPanel);
		JLabel magneticFieldLabel = new JLabel(
				"<html>Nat\u0119\u017Cenie pola<br />magnetycznego [T]</html>");
		magneticFieldSetter = new JTextField();
		magneticFieldSetter.setPreferredSize( new Dimension(90,30) );
		magneticFieldSetter.setMaximumSize( magneticFieldSetter.getPreferredSize() );
		magneticFieldPanel.add(magneticFieldLabel);
		magneticFieldPanel.add(magneticFieldSetter);
		
	}
	
	void createElectricFieldSection() {
		JPanel electricFieldPanel = new JPanel();
		electricFieldPanel.setLayout(new BoxLayout(electricFieldPanel, BoxLayout.LINE_AXIS));
		add(electricFieldPanel);
		JLabel electricFieldLabel = new JLabel(
				"<html>Nat\u0119\u017Cenie pola<br />elektrycznego [V/m]</html>");
		electricFieldSetter = new JTextField();
		electricFieldSetter.setPreferredSize( new Dimension(90,30) );
		electricFieldSetter.setMaximumSize( electricFieldSetter.getPreferredSize() );
		electricFieldPanel.add(electricFieldLabel);
		electricFieldPanel.add(electricFieldSetter);

	}
	
	void createElectricFieldFrequencySection() {
		JPanel electricFieldFrequencyPanel = new JPanel();
		electricFieldFrequencyPanel.setLayout(
				new BoxLayout(electricFieldFrequencyPanel, BoxLayout.LINE_AXIS));
		add(electricFieldFrequencyPanel);
		JLabel electricFieldFrequencyLabel = new JLabel(
				"<html>Cz\u0119stotliwo\u015B\u0107 pola<br />elektrycznego [MHz]</html>");
		electricFieldFrequencySetter = new JTextField();
		electricFieldFrequencySetter.setPreferredSize( new Dimension(90,30) );
		electricFieldFrequencySetter.setMaximumSize( 
				electricFieldFrequencySetter.getPreferredSize() );
		electricFieldFrequencySetter.setToolTipText("" +
				"Wpisz 0 by wybra\u0107 w\u0142a\u015Bciw\u0105 cz\u0119stotliwo\u015B\u0107");
		electricFieldFrequencyPanel.add(electricFieldFrequencyLabel);
		electricFieldFrequencyPanel.add(electricFieldFrequencySetter);

	}
	
	void createControlButtonsSection() {
		JPanel controlButtonsPanel = new JPanel();
		controlButtonsPanel.setLayout(new BoxLayout(controlButtonsPanel, BoxLayout.LINE_AXIS));
		add(controlButtonsPanel);
		startButton = new JButton("Start");
		startButton.setName("start");
		startButton.addActionListener(startButtonListener);
		stopButton = new JButton("Stop");
		stopButton.setName("stop");
		stopButton.setEnabled(false);
		stopButton.addActionListener(stopButtonListener);
		pauseButton = new JButton("Pauza");
		pauseButton.setName("pause");
		pauseButton.setEnabled(false);
		pauseButton.addActionListener(pauseButtonListener);
		clearButton = new JButton("Czy\u015B\u0107");
		clearButton.setName("clear");
		clearButton.setEnabled(false);
		clearButton.addActionListener(clearButtonListener);
		controlButtonsPanel.add(startButton);
 		startButton.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
		controlButtonsPanel.add(stopButton);
 		stopButton.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
		controlButtonsPanel.add(pauseButton);
		pauseButton.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
		controlButtonsPanel.add(clearButton);
		clearButton.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
	}
	
	void createTypeOfParticleSection() {
		String[] particle = {"Elektron", "Proton", "Jon deuteru", "Dowolny pierwiastek"};
		typeOfParticle = new JComboBox<String>(particle);
		typeOfParticle.setAlignmentX(CENTER_ALIGNMENT);
		add(typeOfParticle);
		typeOfParticle.addActionListener(typeOfParticleListener);
		
	}
	
	void createParticleSection(){
		JPanel particlePanel = new JPanel();
		particlePanel.setLayout(new BoxLayout(particlePanel, BoxLayout.LINE_AXIS));
		add(particlePanel);
		particleMassSetter = new JTextField("1");
		JLabel particleMassLabel = new JLabel("<html>Masa cz\u0105stki</html>");
		particleMassSetter.setPreferredSize( new Dimension(140,30) );
		particleMassSetter.setMaximumSize( particleMassSetter.getPreferredSize() );
		particlePanel.add(particleMassLabel);
		particlePanel.add(particleMassSetter);
		particleChargeSetter = new JTextField("1");
		JLabel particleLoadLabel = new JLabel("<html>\u0141adunek cz\u0105stki</html>");
		particleChargeSetter.setPreferredSize( new Dimension(140,30) );
		particleChargeSetter.setMaximumSize( particleMassSetter.getPreferredSize() );
		particlePanel.add(particleLoadLabel);
		particlePanel.add(particleChargeSetter);
		particleMassSetter.setEnabled(false);
		particleChargeSetter.setEnabled(false);
		particleMassSetter.setToolTipText(
				"Wpisz mas\u0119 pierwiastka jako wielokrotno\u015B\u0107 masy elektronu");
		particleChargeSetter.setToolTipText(
				"Wpisz \u0141adunek pierwiastka jako wielokrotno\u015B\u0107 \u0141adunku elektronu");
	}	

	void createAutomaticParamaterButtonSection() {
		automaticParametersButton = new JButton("Automatycznie dobierz parametry");
		automaticParametersButton.setAlignmentX(CENTER_ALIGNMENT);
		add(automaticParametersButton);
		automaticParametersButton.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
		automaticParametersButton.addActionListener(automaticParameterButtnonListener);

	}
	
	void createAnimationSpeedSection() {
		JPanel animationSpeedPanel = new JPanel();
		animationSpeedPanel.setLayout(new BoxLayout(animationSpeedPanel, BoxLayout.LINE_AXIS));
		add(animationSpeedPanel);
		JLabel animationSpeedLabel = new JLabel("Tempo animacji ");
		fasterButton = new JButton("+");
		slowerButton = new JButton("-");
		animationSpeedPanel.add(animationSpeedLabel);
		animationSpeedPanel.add(Box.createHorizontalStrut(5));
		animationSpeedPanel.add(fasterButton);
		fasterButton.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
		animationSpeedPanel.add(slowerButton);
		slowerButton.setBorder(BorderFactory.createEmptyBorder(10,20,10,20));
		fasterButton.addActionListener(fasterSpeedOfAnimationListener);
		slowerButton.addActionListener(slowerSpeedOfAnimationListener);

	}
	
	void createVelocitySection() {
		JPanel velocityPanel = new JPanel();
		velocityPanel.setLayout(new BoxLayout(velocityPanel, BoxLayout.LINE_AXIS));
		add(velocityPanel);
		JLabel velocityLabel = new JLabel(
				"<html>Aktualna pr\u0119dko\u015B\u0107<br />cz\u0105stki [km/s]</html>");
		velocityValue = new JTextField("0");
		velocityValue.setPreferredSize( new Dimension(150,30) );
		velocityValue.setMaximumSize( velocityValue.getPreferredSize() );
		velocityValue.setEditable(false);
		velocityPanel.add(velocityLabel);
		velocityPanel.add(velocityValue);

	}
	
	void createEnergySection() {
		JPanel energyPanel = new JPanel();
		energyPanel.setLayout(new BoxLayout(energyPanel, BoxLayout.LINE_AXIS));
		add(energyPanel);
		JLabel energyLabel = new JLabel("<html>Aktualna energia<br />cz\u0105stki [eV]</html>");
		energyValue = new JTextField("0");
		energyValue.setPreferredSize( new Dimension(150,30) );
		energyValue.setMaximumSize( energyValue.getPreferredSize() );
		energyValue.setEditable(false);
		energyPanel.add(energyLabel);
		energyPanel.add(energyValue);
	}
	
	ActionListener typeOfParticleListener = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			int ii = typeOfParticle.getSelectedIndex();
			
			if(ii==ELECTRON_SELECTED)
			{	
				particleMassSetter.setText("1");
				particleChargeSetter.setText("1");
				particleMassSetter.setEnabled(false);
				particleChargeSetter.setEnabled(false);
				automaticParametersButton.setEnabled(true);

			}
			
			if(ii==PROTON_SELECTED)
			{	
				particleMassSetter.setText("1836");
				particleChargeSetter.setText("1");
				particleMassSetter.setEnabled(false);
				particleChargeSetter.setEnabled(false);
				automaticParametersButton.setEnabled(true);

			}
			
			if(ii==DEUTER_SELECTED)
			{	
				particleMassSetter.setText("3670");
				particleChargeSetter.setText("1");
				particleMassSetter.setEnabled(false);
				particleChargeSetter.setEnabled(false);
				automaticParametersButton.setEnabled(true);

			}
			
			if(ii==ANOTHER_SELECTED)
			{
				particleMassSetter.setEnabled(true);
				particleChargeSetter.setEnabled(true);
				automaticParametersButton.setEnabled(false);
			}
			
		}
		
	};
	
	ActionListener startButtonListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				inputDataValidation();
				startComputing();
				disableSetters();
				stopButton.setEnabled(true);
				pauseButton.setEnabled(true);
				startButton.setEnabled(false);
				parrent.getDrawArea().initialiseImage();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(parrent, e1.getMessage(), "Problem z danymi", 
						JOptionPane.ERROR_MESSAGE);
			}
		}
	};
	
	ActionListener stopButtonListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			stopButtonDisablingAction();
			computePoints.interrupt();
			ChartsPanel.timer.stop();
			}
	};

	ActionListener pauseButtonListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			pauseButton.setText("Wzn\u00F3w");
			pauseButton.setName("resume");
			pauseButton.removeActionListener(pauseButtonListener);
			pauseButton.addActionListener(resumeButtonListener);
			computePoints.pause();
			ChartsPanel.timer.stop();
		}
	};
	
	ActionListener resumeButtonListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			pauseButton.setText("Pauza");
			pauseButton.setName("pause");
			pauseButton.removeActionListener(resumeButtonListener);
			pauseButton.addActionListener(pauseButtonListener);
			createComputeThread(
					particleChargeOverMass, chargeFactor,
					electricField, magneticField, electricFieldFrequency);
		}
	};

	ActionListener clearButtonListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			enableSetters();
			startButton.setEnabled(true);
			clearButton.setEnabled(false);
			ComputeThread.clearVariables();
			parrent.getDrawArea().setAnimationImage(null);
			parrent.setPointList(null);
			parrent.getDrawArea().setVectorStart(0, 0);
			parrent.getDrawArea().setVectorEnd(0, 0);
			parrent.getDrawArea().initialiseImage();
			parrent.getDrawArea().repaint();
			parrent.getCharts().setNullCharts();
		}
	};
	
	ActionListener automaticParameterButtnonListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			int ii = typeOfParticle.getSelectedIndex();
			
			if(ii==ELECTRON_SELECTED)
			{	
				electricFieldSetter.setText("17500");
				magneticFieldSetter.setText("0.00625");
				electricFieldFrequencySetter.setText("0");
			}
			
			if(ii==PROTON_SELECTED)
			{	
				electricFieldSetter.setText("17500");
				magneticFieldSetter.setText("0.925");
				electricFieldFrequencySetter.setText("28");
			}
			
			if(ii==DEUTER_SELECTED)
			{	
				electricFieldSetter.setText("1750000");
				magneticFieldSetter.setText("2");
				electricFieldFrequencySetter.setText("90");
			}
		}
	};
	
	ActionListener slowerSpeedOfAnimationListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(speedSelected == 0) 
				fasterButton.setEnabled(true);
			++speedSelected;
			if (speedSelected == speedOption.length - 1)
				slowerButton.setEnabled(false);
			ComputeThread.setDelay(speedOption[speedSelected]);
		}
	};
	
	ActionListener fasterSpeedOfAnimationListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (speedSelected == speedOption.length - 1)
				slowerButton.setEnabled(true);
			--speedSelected;
			if (speedSelected == 0)
				fasterButton.setEnabled(false);
			ComputeThread.setDelay(speedOption[speedSelected]);
		}
	};

	
	void startComputing() throws Exception {
		electricField = Double.parseDouble(electricFieldSetter.getText());
		magneticField = Double.parseDouble(magneticFieldSetter.getText());
		electricFieldFrequency = Double.parseDouble(electricFieldFrequencySetter.getText());
		particleChargeOverMass = 
				(Integer.parseInt(particleChargeSetter.getText()) * chargeElectron) /
				(Double.parseDouble(particleMassSetter.getText()) * massElectron) *
				Math.pow(10, 12);
		chargeFactor = Double.parseDouble(particleChargeSetter.getText());
		try {
			checkingDisaster(electricField, magneticField, particleChargeOverMass);
			createComputeThread(particleChargeOverMass, chargeFactor, 
					electricField, magneticField, electricFieldFrequency);
		} catch (Exception e){
			throw e;
		}
	}
	
	void createComputeThread(double particleChargeOverMass, double chargeFactor, 
			double electricField, double magneticField, double electricFieldFrequency) {
		computePoints = new ComputeThread(parrent, particleChargeOverMass, chargeFactor, 
				electricField, magneticField, electricFieldFrequency);
		computePoints.start();
		parrent.getCharts().animation();

	}
	
	void stopButtonDisablingAction() {
		clearButton.setEnabled(true);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(false);
		if (pauseButton.getName().equals("resume")) {
			pauseButton.setText("Pauza");
			pauseButton.setName("pause");
			pauseButton.removeActionListener(resumeButtonListener);
			pauseButton.addActionListener(pauseButtonListener);
		}

	}
	
	void disableSetters() {
		magneticFieldSetter.setEnabled(false);
		electricFieldSetter.setEnabled(false);
		electricFieldFrequencySetter.setEnabled(false);
		typeOfParticle.setEnabled(false);
		particleMassSetter.setEnabled(false);
		particleChargeSetter.setEnabled(false);
		automaticParametersButton.setEnabled(false);
		
	}
	
	void enableSetters() {
		magneticFieldSetter.setEnabled(true);
		electricFieldSetter.setEnabled(true);
		electricFieldFrequencySetter.setEnabled(true);
		typeOfParticle.setEnabled(true);
		if(typeOfParticle.getSelectedIndex() != ANOTHER_SELECTED) 
			automaticParametersButton.setEnabled(true);
		if(typeOfParticle.getSelectedIndex() == ANOTHER_SELECTED) {
			particleMassSetter.setEnabled(true);
			particleChargeSetter.setEnabled(true);
		}
	}
	
	void inputDataValidation () throws Exception {
		try {
			if((Double.parseDouble(electricFieldSetter.getText())) < 0 ||
			(Double.parseDouble(magneticFieldSetter.getText())) < 0 ||
			Double.parseDouble(electricFieldFrequencySetter.getText()) < 0)
				throw new Exception("Parametry p\u00F3l musz\u0105 by\u0107 dodatnie!");
			if(Double.parseDouble(particleChargeSetter.getText()) < 1 ||
			Double.parseDouble(particleMassSetter.getText()) < 1) 
				throw new Exception(
						"Parametry cz\u0105stki musz\u0105 by\u0107 wielokrotno\u015Bci\u0105 parametr\u00F3w elekronu (liczba powinna by\u0107 wi\u0119ksza od 1)!");
		} catch (NumberFormatException e){
			throw new Exception(
					"Nieprawid\u0142owe symbole. Wprowadzaj wy\u0142\u0105cznie liczby!");
		}
		try {
			Integer.parseInt(particleChargeSetter.getText());
		} catch (NumberFormatException e){
			throw new Exception(
					"\u0141adunek cz\u0105stki musi by\u0107 ca\u0142kowit\u0105 wielokrotno\u015Bci\u0105 \u0142adunku elektronu (liczba powinna by\u0107 ca\u0142kowita i wi\u0119ksza od 1)!");
		}
	}
	
	void checkingDisaster(double electricField, double magneticField, double chargeOverMass) throws Exception {
		if (magneticField * ComputeThread.maxRadius * chargeOverMass > 200000000)
			throw new Exception(
					"Uwaga - efekty relatywistyczne! Zmniejsz nat\u0119\u017Cenie pola elektrycznego i/lub magnetycznego");
        double startVelocity = ComputeThread.initialVelocity + Math.sqrt(
        		2 * chargeOverMass * electricField * ComputeThread.space * ComputeThread.spaceUnit);
        double startRadius = startVelocity/(magneticField*chargeOverMass);
        if (startRadius < 0.03 * ComputeThread.maxRadius) 
			throw new Exception(
					"Uwaga - d\u0142ugie kr\u0105\u017Cenie cz\u0105stki w cyklotronie! Zwi\u0119ksz nat\u0119\u017Cenie pola elektrycznego lub zmniejsz nat\u0119\u017Cenie pola magnetycznego");
        if (startRadius > 0.3 * ComputeThread.maxRadius) 
			throw new Exception(
					"Uwaga - cz\u0105stka ucieknie z cylotronu zbyt szybko! zmniejsz nat\u0119\u017Cenie pola elektrycznego lub zwi\u0119ksz nat\u0119\u017Cenie pola magnetycznego");

	
	}

}
