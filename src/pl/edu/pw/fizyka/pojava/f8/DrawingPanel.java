package pl.edu.pw.fizyka.pojava.f8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

//Class creates drawing panel, ciclotrone and vector. It also redraw image when window resized.
//Methods for drawing animation. 

public class DrawingPanel extends JPanel {
	private static final long serialVersionUID = 1952056547579739479L;
	private ContentApplet parrent;
	private final int margin = 5;
	private int space;
	private int ciclotroneRadius;
	private BufferedImage animationImage;
	private Graphics2D animationGraphics;
	private Point2D vectorStart = new java.awt.Point(0,0);
	private Point2D vectorEnd = new java.awt.Point(0,0);
	private Point center;
	
	public BufferedImage getAnimationImage() {
		return animationImage;
	}
	
	public void setAnimationImage(BufferedImage animationImage) {
		this.animationImage = animationImage;
	}

	private void setSpace () {
		space = ciclotroneRadius / 20;
	}
	private void setCenter () {
		center.x = getSize().width/2;
		center.y = getSize().height/2;
	}
	
	public java.awt.Point getCenter() {
		return center;
	}

	public void setVectorStart(double x, double y) {
		vectorStart = new Point2D.Double(x,y);
		}

	public void setVectorEnd(double x, double y) {
		vectorEnd = new Point2D.Double(x,y);
	}
	
	private Point translatePoint (Point2D point) {
		Point translated = new Point((int)(point.getX()*ciclotroneRadius/0.2) + center.x, 
				(int)(point.getY()*ciclotroneRadius/0.2) + center.y);
		return translated;
	}

	public DrawingPanel(ContentApplet parrent) {
		this.parrent = parrent;
		center = new java.awt.Point();
		setBackground(Color.WHITE);
		addComponentListener(windowChanges);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(animationImage, 0, 0, this);
		if (!vectorStart.equals(vectorEnd)) {
			g.setColor(Color.GREEN);
			drawVector(g);
		}
	}
	
	ComponentAdapter windowChanges =  new ComponentAdapter() {
		
		@Override
		public void componentResized(ComponentEvent e) {
			initialiseImage();
			redraw();
			repaint();
		}
	};
	
	//Method reinitialize the image when we create new animation or resize window

	public void initialiseImage () {
		setCenter();
		animationImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		animationGraphics = animationImage.createGraphics();
		drawCiclotron(animationGraphics);
		
	}

	
	private void redraw() {
		if (parrent.getPointList() != null) {
			synchronized (parrent) {
				for (int ii = 0; ii < parrent.getPointList().size()-1; ++ii)
					drawPoint(parrent.getPointList().get(ii), parrent.getPointList().get(ii+1));		
			}
		}
	}
	
	private void drawLine(int firstPointX, int firstPointY, int secondPointX, int secondPointY) {
		animationGraphics.drawLine(firstPointX, firstPointY, secondPointX, secondPointY);
	}
	
	//Method draw animation points
	
	public void drawPoint(AnimationPoint current, AnimationPoint next) {
		animationGraphics.setColor(Color.BLACK);
		drawLine(
				(int)(current.getxFromCenter()*ciclotroneRadius/0.2) + center.x,
				(int)(current.getyFromCenter()*ciclotroneRadius/0.2)  + center.y,
				(int)(next.getxFromCenter()*ciclotroneRadius/0.2)  + center.x,
				(int)(next.getyFromCenter()*ciclotroneRadius/0.2)  + center.y);
	}

	private void drawCiclotron(Graphics2D g) {
		int x,y;
		ciclotroneRadius = getHeight()/2 - margin;
		setSpace();
		x = center.x - ciclotroneRadius;
		y = center.y - ciclotroneRadius;
		animationGraphics.setColor(Color.RED);
		g.fill(new Arc2D.Double(
				x - space, y,2*ciclotroneRadius,2*ciclotroneRadius,90, 180,Arc2D.PIE));
		g.fill(new Arc2D.Double(x, y,2*ciclotroneRadius,2*ciclotroneRadius,-90,180,Arc2D.PIE));	
	}

	//Method creates velocity vector

	public void drawVector(Graphics g){
		double x[] = new double[3];
        double y[] = new double[3];
        Point start = translatePoint(vectorStart);
        Point end = translatePoint(vectorEnd);
        double arc = Math.atan2(-end.getY() + start.getY(), end.getX() - start.getX());
        double length = Math.sqrt(
        		Math.pow(end.getX() - start.getX(), 2) + 
        		Math.pow(end.getY() - start.getY(), 2)) * 0.85;
        x[0] = start.getX() + Math.round(length * Math.cos(arc + 0.25));
        y[0] = start.getY() + Math.round(length * -Math.sin(arc + 0.25));
        x[1] = start.getX() + Math.round(length * Math.cos(arc - 0.25));
        y[1] = start.getY() + Math.round(length * -Math.sin(arc - 0.25));
        x[2] = end.getX();
        y[2] = end.getY();
        g.drawLine((int) start.getX(), (int) start.getY(), (int) end.getX(), (int) end.getY());
        g.drawLine((int) x[0], (int) y[0], (int) x[2], (int) y[2]);
        g.drawLine((int) x[1], (int) y[1], (int) x[2], (int) y[2]);
			
	}


}

