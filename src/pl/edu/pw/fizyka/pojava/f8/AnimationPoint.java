package pl.edu.pw.fizyka.pojava.f8;

//Class creates point properties

public class AnimationPoint {
	double xFromCenter, yFromCenter;
	public double getxFromCenter() {
		return xFromCenter;
	}
	public double getyFromCenter() {
		return yFromCenter;
	}
	public double getVelocity() {
		return velocity;
	}
	public double getTime() {
		return time;
	}
	public double getEnergy() {
		return energy;
	}
	double velocity, time, energy;
	public AnimationPoint(double xFromCenter, double yFromCenter, double time, 
			double velocity, double energy) {
		this.xFromCenter = xFromCenter;
		this.yFromCenter = yFromCenter;
		this.time = time;
		this.velocity = velocity;
		this.energy = energy;
	}
}
