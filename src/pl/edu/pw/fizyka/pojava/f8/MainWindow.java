package pl.edu.pw.fizyka.pojava.f8;

import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.UIManager;

//Class creates main window

public class MainWindow extends JFrame {
	
	private static final long serialVersionUID = -8290161341000538411L;
	static ContentApplet applet;
	
	//Method creates the appearance of the main window
	
	public MainWindow() throws HeadlessException {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		setTitle("Cyklotron");
		setSize(1024, 768);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		applet = new ContentApplet();
		setContentPane(applet.getContentPane());
		applet.init();		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MainWindow mainWindow = new MainWindow();
		mainWindow.setVisible(true);
		applet.start();

	}

}
